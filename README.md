# Fungipedia

This is a simple node express app to inspect the content of my local directory of mushroom pictures
and ease their classification. It offers a home page to navigate over all the directory content,
plus a detail page to add metadata to a particular picture.

The metadata is saved in the app memory, and once the app exits, the internal data is saved as JSON
file.

## Getting started

To start the application, first install the dependencies:

```
npm i
```

And run the local server:

```
npm start
```

## Media directory

Place the pictures under the folder `assets/media`


## Wishlist

* [ ] Change file name
* [ ] Autofill date and location from file metadata? ^^U
* [ ] Containerize!
