import express from 'express'
import { fileURLToPath } from 'url'
import path from 'path'
import fs from 'fs'

import { Liquid } from 'liquidjs'
import { filters } from './src/liquid.js'

const PORT = process.env.LISTENING_PORT || 5050
const INSTALL_PATH = path.dirname(fileURLToPath(import.meta.url))
const STATIC_PATH = path.join(INSTALL_PATH, 'assets')
const MEDIA_PATH = path.join(STATIC_PATH, 'media')
const VIEWS_PATH = path.join(INSTALL_PATH, 'views')
const PAGE_ITEMS = 12

const app = express()
const engine = new Liquid()

// Engine filters
engine.registerFilter('join', filters.join)
engine.registerFilter('slugify', filters.slugify)

app.use(express.static(STATIC_PATH))
app.use(express.json())
app.use(express.urlencoded())

// Settings
app.engine('liquid', engine.express())
app.set('views', VIEWS_PATH)
app.set('view engine', 'liquid')

let db = {}

app.get('/', (req, res) => {
  const search = req.query.search ? req.query.search.toLowerCase() : ''
  const filenames = getVisibleFilenames(db.data, search)
  const page = req.query.page ? parseInt(req.query.page) : 1
  const keys = Object.keys(filenames)
  const count = keys.length
  const pages = Math.ceil(count/PAGE_ITEMS)
  return res.render('index', {
    pagination: {
      page: page,
      count: count,
      pages: pages,
      limit: PAGE_ITEMS,
      offset: PAGE_ITEMS * (page-1),
      previous: (page-1),
      next: (page+1)
    },
    filenames: filenames,
    data: db.data,
    search: search
  })
})

app.delete('/:filename', (req, res) => {
  db.data[req.params.filename]['hidden'] = true
  return res.sendStatus(200)
})

app.get('/:filename', (req, res) => {
  const keys = getVisibleFilenames(db.data)
  const index = keys.indexOf(req.params.filename)
  const prevIndex = index-1
  const nextIndex = index+1
  const previous = (prevIndex < 0) ? keys[0] : keys[prevIndex]
  const next = (nextIndex < keys.length) ? keys[nextIndex] : keys[keys.length - 1]
  return res.render('mushroom', {
    filename: req.params.filename,
    previousData: db.data[previous],
    data: db.data[req.params.filename],
    page: Math.floor(index / PAGE_ITEMS) + 1,
    previous: previous,
    next: next
  })
})

app.post('/:filename', (req, res) => {
  db.data[req.params.filename] = req.body
  db.data[req.params.filename].other_names = stringToArray(req.body['other_names'])
  db.data[req.params.filename].tags = stringToArray(req.body['tags'])
  const next = req.query.next
  return res.redirect(`/${next}`)
})

app.get('/search/:term', (req, res) => {
  const term = req.params.term.toLomerCase()
  const response = {}
  for (const key in db.data) {
    const data = db.data[key]
    if ( data.hidden ) continue;
    if (
      (data.scientific_name && (data.scientific_name.toLowerCase().indexOf(term) > -1)) ||
      (data.other_names && (data.other_names.join(' ').toLowerCase().indexOf(term) > -1)) ||
      (data.tags && (data.tags.join(' ').toLowerCase().indexOf(term) > -1 ))
    ) {
      response[key] = data;
    }
  }
  return res.json(response)
})

function getVisibleFilenames(data, term = false ) {
  if (term) return getFilteredFilenames(data, term)
  return Object.keys(data).filter((k) => {
    return !data[k].hidden
  })
}

function getFilteredFilenames(data, term) {
  return Object.keys(data).filter((k) => {
    if (data[k].hidden) return false
    return (
      (data[k].scientific_name && (data[k].scientific_name.toLowerCase().indexOf(term) > -1)) ||
      (data[k].other_names && (data[k].other_names.join(' ').toLowerCase().indexOf(term) > -1)) ||
      (data[k].tags && (data[k].tags.join(' ').toLowerCase().indexOf(term) > -1 ))
    )
  })
}

function stringToArray(value) {
  return value
    ? value.split(',').map(e => e.trim())
    : []
}

function init() {
  console.log(`Loading data file`)
  try {
    const file = fs.readFileSync(path.join(INSTALL_PATH, 'data.json'), 'utf8')
    db.data = JSON.parse(file)
  } catch (err) {
    db.data = {}
  }

  console.log(`Loading media dir`)
  // TODO mark data that isn't found in the filesystem (as hidden?)
  fs.readdir(MEDIA_PATH, (err, files) => {
    if (err) {
      console.log(err)
    }

    for (const file of files) {
      if (file.charAt(0) !== '.' && !db.data[file]) {
        db.data[file] = {}
      }
    }
  })
}

async function cleanup() {
  console.log(`Saving data`)

  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(INSTALL_PATH, 'data.json'), JSON.stringify(db.data, null, 2), (err) => {
      if (err) {
        return console.log(err)
        reject()
      }
      console.log(`The file was saved!`)
      resolve()
    })
  })
}

init()

app.listen(PORT, () => {
	console.log(`Listening on http://localhost:${PORT}`)
})

// Catch ctrl+c event and exit normally
process.on('SIGINT', async function () {
	console.log('\nTerminated!')
	await cleanup()
	process.exit(2)
})

// Catch uncaught exceptions, trace, then exit normally
process.on('uncaughtException', async function(e) {
	console.log('\nUncaught Exception!')
	console.log(e.stack)
	await cleanup()
	process.exit(99)
})
