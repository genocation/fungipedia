export const filters = {
  join: function(list) {
    if (list) {
      return list.join(', ')
    }
    return ''
  },
  slugify: function(filename) {
    return filename.split('.').join('-')
  }
}

